# Presenter

> Application for presenting media created an app with [Electron](https://github.com/electron/electron)

## Features

- Display your videos(MP4's) and images (JPG's,PNG's)
- Style your media with usage of CSS filters
- Sort your media with interactive media list


## Getting started

Required dependency manager is Yarn

Copy current repository with

```
$ git copy https://gitlab.com/celtol/presenter.git "your folder name"
```

## Install

For instalation you need configured FontAwesome Pro Licence for project or your environment [Font Awesome Pro Licence Configuration](https://fontawesome.com/how-to-use/on-the-web/setup/using-package-managers)

---
After copying of repository run:
```
yarn
```
and next use:
```
yarn pack
yarn dist
```
for packing the newest version of application, and you will find you packaged vesrsions and setup's in dist folder

## Dev

After copying of repository run:
```
yarn
```
and next use:
```
yarn start
```
for further usage as developer, without installing and packaging the application

