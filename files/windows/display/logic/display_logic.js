let events = require("electron").remote.app.Event
let remote = require("electron").remote

let $ = require('jquery');

let currentType
let videoPaused = 0

events.on("itemToDisplay", (m) => {
	if (m.type.includes('image/')) {
		let imgData = JSON.parse(JSON.stringify(m))
		loadImage(imgData)
	} else if (m.type.includes('video/')) {
		let videoData = JSON.parse(JSON.stringify(m))
		loadVideo(videoData)
	}
})

events.on("stopItemDisplay", () => {
	stopDisplay()
})

events.on("pauseDisplay", () => {
	pauseDisplay()
})

function loadImage(data) {
	if (currentType == "image") {
		$("#image_div").fadeOut(500)
		setTimeout(() => {
			$("#image_div").fadeIn(500)
			$("#image_tag").css("filter", "blur(" + data.style.blur + "px) brightness(" + data.style.brightness + "%) contrast(" + data.style.contrast + "%) grayscale(" + data.style.grayscale + "%) hue-rotate(" + data.style.huerotate + "deg) invert(" + data.style.invert + "%) opacity(" + data.style.opacity + "%) saturate(" + data.style.saturate + "%) sepia(" + data.style.sepia + "%)")
			$("#image_tag").attr("src", "")
			$("#image_tag").attr("src", data.path.replace(/\\/g, "/"))
		}, 500);
	} else if (currentType == "video") {
		currentType = "image"
		$("#video_div").fadeOut(500)
		setTimeout(() => {
			$("#image_div").fadeIn(500)
			$("#image_tag").css("filter", "blur(" + data.style.blur + "px) brightness(" + data.style.brightness + "%) contrast(" + data.style.contrast + "%) grayscale(" + data.style.grayscale + "%) hue-rotate(" + data.style.huerotate + "deg) invert(" + data.style.invert + "%) opacity(" + data.style.opacity + "%) saturate(" + data.style.saturate + "%) sepia(" + data.style.sepia + "%)")
			$("#image_tag").attr("src", "")
			$("#image_tag").attr("src", data.path.replace(/\\/g, "/"))
		}, 500);
	} else {
		currentType = "image"
		$("#image_div").fadeIn(500)
		$("#image_tag").css("filter", "blur(" + data.style.blur + "px) brightness(" + data.style.brightness + "%) contrast(" + data.style.contrast + "%) grayscale(" + data.style.grayscale + "%) hue-rotate(" + data.style.huerotate + "deg) invert(" + data.style.invert + "%) opacity(" + data.style.opacity + "%) saturate(" + data.style.saturate + "%) sepia(" + data.style.sepia + "%)")
		$("#image_tag").attr("src", "")
		$("#image_tag").attr("src", data.path.replace(/\\/g, "/"))
	}
}

function loadVideo(data) {
	if (currentType == "video") {
		$("#video_div").fadeOut(500)
		setTimeout(() => {
			$("#video_div").fadeIn(500)
			$("#video_tag").css("filter", "blur(" + data.style.blur + "px) brightness(" + data.style.brightness + "%) contrast(" + data.style.contrast + "%) grayscale(" + data.style.grayscale + "%) hue-rotate(" + data.style.huerotate + "deg) invert(" + data.style.invert + "%) opacity(" + data.style.opacity + "%) saturate(" + data.style.saturate + "%) sepia(" + data.style.sepia + "%)")
			$("#video_tag").attr("src", data.path.replace(/\\/g, "/"))
			$("#video_tag")[0].play()
		}, 500);
	} else if (currentType == "image") {
		currentType = "video"
		$("#image_div").fadeOut(500)
		setTimeout(() => {
			$("#video_div").fadeIn(500)
			$("#video_tag").css("filter", "blur(" + data.style.blur + "px) brightness(" + data.style.brightness + "%) contrast(" + data.style.contrast + "%) grayscale(" + data.style.grayscale + "%) hue-rotate(" + data.style.huerotate + "deg) invert(" + data.style.invert + "%) opacity(" + data.style.opacity + "%) saturate(" + data.style.saturate + "%) sepia(" + data.style.sepia + "%)")
			$("#video_tag").attr("src", data.path.replace(/\\/g, "/"))
			$("#video_tag")[0].play()
		}, 500);
	} else {
		currentType = "video"
		$("#video_div").fadeIn(500)
		$("#video_tag").css("filter", "blur(" + data.style.blur + "px) brightness(" + data.style.brightness + "%) contrast(" + data.style.contrast + "%) grayscale(" + data.style.grayscale + "%) hue-rotate(" + data.style.huerotate + "deg) invert(" + data.style.invert + "%) opacity(" + data.style.opacity + "%) saturate(" + data.style.saturate + "%) sepia(" + data.style.sepia + "%)")
		$("#video_tag").attr("src", data.path.replace(/\\/g, "/"))
		$("#video_tag")[0].play()
	}
}

function pauseDisplay() {
	if (currentType == "video") {
		if (videoPaused == 0) {
			$("#video_tag")[0].pause()
			videoPaused = 1
		} else if (videoPaused == 1) {
			$("#video_tag")[0].play()
			videoPaused = 0
		}
	}
}

function stopDisplay() {
	if (currentType == "image") {
		$("#image_div").hide()
		$("#image_tag").attr("src", "")
		currentType = ""
	} else if (currentType == "video") {
		$("#video_div").hide()
		$("#video_tag")[0].load()
		$("#video_tag").attr("src", "")
		videoPaused = 0
		currentType = ""
	}
}

$(document).on('keydown', (evt) => {
	console.log(evt.key);
	switch (evt.key) {
		case "Escape":
			if (remote.getCurrentWindow().isFullScreen()) {
				remote.getCurrentWindow().setFullScreen(false);
				if (remote.getCurrentWindow().isMaximized()) {
					remote.getCurrentWindow().unmaximize()
					setTimeout(() => {
						if (remote.getCurrentWindow().isFullScreen()) {
							remote.getCurrentWindow().setFullScreen(false);
						}
					}, 100);
				}
			}
			break;
	}
})
