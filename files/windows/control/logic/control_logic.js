let events = require("electron").remote.app.Event

let $ = require('jquery');
require('popper.js');
require('bootstrap');
let Sortable = require('sortablejs')

let mediaState = 2;
let playlist = [];
let playlistItem = 0;
let videoPaused = 0
let currentType
let imageTimer


function startTimer() {
	if (currentType = "image") {
		imageTimer = setTimeout(() => {
			checkOnNext()
		}, parseInt(playlist[playlistItem].timer) * 1000);
	}
}

async function openFiles(data) {
	let files_data = data.target.files
	for (let index = 0; index < files_data.length; index++) {
		const element = files_data[index];
		if (element.type.includes("image/")) {
			let timer = await getTimer(element)
			if (timer.add == "yes") {
				let itemId = playlist.length
				element.timer = timer.time
				element.id = itemId
				element.style = {
					blur: 0,
					brightness: 100,
					contrast: 100,
					grayscale: 0,
					huerotate: 0,
					invert: 0,
					opacity: 100,
					saturate: 100,
					sepia: 0
				}
				playlist.push(element)
				addToList(itemId, element.name)
			}
		} else if (element.type.includes("video/")) {
			let itemId = playlist.length
			element.id = itemId
			element.style = {
				blur: 0,
				brightness: 100,
				contrast: 100,
				grayscale: 0,
				huerotate: 0,
				invert: 0,
				opacity: 100,
				saturate: 100,
				sepia: 0
			}
			playlist.push(element)
			addToList(itemId, element.name)
		}
	}
}

async function styleItem(id) {
	let itemId = id
	let style = await getStyle(itemId)
	console.log(style);

	playlist[id].style = style
}

function getStyle(id) {
	let itemId = id
	openStylingModal(itemId)
	return new Promise(resolve => {
		$("#save_style").on("click", () => {
			let style = {
				blur: document.getElementById("blur_input").value,
				brightness: document.getElementById("brightness_input").value,
				contrast: document.getElementById("contrast_input").value,
				grayscale: document.getElementById("grayscale_input").value,
				huerotate: document.getElementById("huerotate_input").value,
				invert: document.getElementById("invert_input").value,
				opacity: document.getElementById("opacity_input").value,
				saturate: document.getElementById("saturate_input").value,
				sepia: document.getElementById("sepia_input").value
			}
			closeStylingModal()
			resolve(style)
		})
	})
}

function addToList(itemId, itemName) {
	let item_list = document.getElementById("item_list")
	let li = document.createElement("li")
	let controlDiv = document.createElement("div")
	let butDiv = document.createElement("div")
	let styleButton = document.createElement("button")
	let deleteButton = document.createElement("button")
	let p = document.createElement("p")

	li.setAttribute("data-id", "" + itemId + "")
	controlDiv.setAttribute("ondblclick", "playItem(" + itemId + ")")
	controlDiv.setAttribute("onclick", "selectItem(" + itemId + ")")
	controlDiv.setAttribute("class", "itemDesc")
	butDiv.setAttribute("class", "itemButo")

	styleButton.innerHTML = '<i class="fad fa-pen"></i>'
	deleteButton.innerHTML = '<i class="fad fa-trash-alt"></i>'
	styleButton.setAttribute("onclick", "styleItem(" + itemId + ")")
	deleteButton.setAttribute("onclick", "deleteItem(" + itemId + ")")

	styleButton.className = "btn btn-itemlisbutt"
	deleteButton.className = "btn btn-itemlisbutt"


	p.innerHTML = itemName
	controlDiv.appendChild(p)
	butDiv.appendChild(styleButton)
	butDiv.appendChild(deleteButton)

	li.appendChild(controlDiv)
	li.appendChild(butDiv)

	item_list.appendChild(li)
}

function deleteItem(id) {
	let itemId = id
	playlist.splice(id, 1)
	refreshPlaylist()
}

function selectItem(itemId) {
	let itemsList = document.getElementsByTagName("li")
	for (let index = 0; index < itemsList.length; index++) {
		const element = itemsList[index];
		const child = itemsList[index].children[0].children[0]
		if (element.dataset.id == itemId) {
			if (child.className.includes("current")) {
				child.className = "current choosen"

			}
		} else {
			if (child.className.includes("current")) {
				child.className = "current"
			} else {
				child.className = ""
			}
		}
	}
}

function playItem(itemId) {
	for (let index = 0; index < playlist.length; index++) {
		const element = playlist[index];
		if (element.id == itemId) {
			playlistItem = index
			events.emit("itemToDisplay", {
				type: element.type,
				path: element.path,
				style: element.style
			})
			itemToPreviev({
				type: element.type,
				path: element.path,
				style: element.style
			})
			$("#play_pause_button").html('<i class="fad fa-pause"></i> Pause')
			mediaState = 1
		}
	}
	markCurrent()
}

function itemToPreviev(m) {
	if (m.type.includes('image/')) {
		let imgData = JSON.parse(JSON.stringify(m))
		loadImage(imgData)
	} else if (m.type.includes('video/')) {
		let videoData = JSON.parse(JSON.stringify(m))
		loadVideo(videoData)
	}
}

function loadImage(data) {
	if (currentType == "image") {
		$("#image_previev_div").fadeOut(500)
		setTimeout(function () {
			$("#image_previev_div").fadeIn(500)
			startTimer()
			$("#image_previev_tag").css("filter", "blur(" + data.style.blur + "px) brightness(" + data.style.brightness + "%) contrast(" + data.style.contrast + "%) grayscale(" + data.style.grayscale + "%) hue-rotate(" + data.style.huerotate + "deg) invert(" + data.style.invert + "%) opacity(" + data.style.opacity + "%) saturate(" + data.style.saturate + "%) sepia(" + data.style.sepia + "%)")
			$("#image_previev_tag").attr("src", "")
			$("#image_previev_tag").attr("src", data.path.replace(/\\/g, "/"))
		}, 500)
	} else if (currentType == "video") {
		currentType = "image"
		$("#video_previev_div").fadeOut(500)
		setTimeout(() => {
			$("#image_previev_div").fadeIn(500)
			startTimer()
			$("#image_previev_tag").css("filter", "blur(" + data.style.blur + "px) brightness(" + data.style.brightness + "%) contrast(" + data.style.contrast + "%) grayscale(" + data.style.grayscale + "%) hue-rotate(" + data.style.huerotate + "deg) invert(" + data.style.invert + "%) opacity(" + data.style.opacity + "%) saturate(" + data.style.saturate + "%) sepia(" + data.style.sepia + "%)")
			$("#image_previev_tag").attr("src", "")
			$("#image_previev_tag").attr("src", data.path.replace(/\\/g, "/"))
		}, 500);
	} else if (typeof currentType == "undefined") {
		currentType = "image"
		$("#image_previev_div").fadeIn(500)
		startTimer()
		$("#image_previev_tag").css("filter", "blur(" + data.style.blur + "px) brightness(" + data.style.brightness + "%) contrast(" + data.style.contrast + "%) grayscale(" + data.style.grayscale + "%) hue-rotate(" + data.style.huerotate + "deg) invert(" + data.style.invert + "%) opacity(" + data.style.opacity + "%) saturate(" + data.style.saturate + "%) sepia(" + data.style.sepia + "%)")
		$("#image_previev_tag").attr("src", "")
		$("#image_previev_tag").attr("src", data.path.replace(/\\/g, "/"))
	}
}

function loadVideo(data) {
	if (currentType == "video") {
		$("#video_previev_div").fadeOut(500)
		setTimeout(function () {
			$("#video_previev_div").fadeIn(500)
			$("#video_previev_tag").css("filter", "blur(" + data.style.blur + "px) brightness(" + data.style.brightness + "%) contrast(" + data.style.contrast + "%) grayscale(" + data.style.grayscale + "%) hue-rotate(" + data.style.huerotate + "deg) invert(" + data.style.invert + "%) opacity(" + data.style.opacity + "%) saturate(" + data.style.saturate + "%) sepia(" + data.style.sepia + "%)")
			$("#video_previev_tag").attr("src", "")
			$("#video_previev_tag").attr("src", data.path.replace(/\\/g, "/"))
			$("#video_previev_tag")[0].play()
		}, 500)
	} else if (currentType == "image") {
		currentType = "video"
		$("#image_previev_div").fadeOut(500)
		setTimeout(() => {
			$("#video_previev_div").fadeIn(500)
			$("#video_previev_tag").css("filter", "blur(" + data.style.blur + "px) brightness(" + data.style.brightness + "%) contrast(" + data.style.contrast + "%) grayscale(" + data.style.grayscale + "%) hue-rotate(" + data.style.huerotate + "deg) invert(" + data.style.invert + "%) opacity(" + data.style.opacity + "%) saturate(" + data.style.saturate + "%) sepia(" + data.style.sepia + "%)")
			$("#video_previev_tag").attr("src", "")
			$("#video_previev_tag").attr("src", data.path.replace(/\\/g, "/"))
			$("#video_previev_tag")[0].play()
		}, 500);
	} else if (typeof currentType == "undefined") {
		currentType = "video"

		$("#video_previev_div").fadeIn(500)
		$("#video_previev_tag").css("filter", "blur(" + data.style.blur + "px) brightness(" + data.style.brightness + "%) contrast(" + data.style.contrast + "%) grayscale(" + data.style.grayscale + "%) hue-rotate(" + data.style.huerotate + "deg) invert(" + data.style.invert + "%) opacity(" + data.style.opacity + "%) saturate(" + data.style.saturate + "%) sepia(" + data.style.sepia + "%)")
		$("#video_previev_tag").attr("src", "")
		$("#video_previev_tag").attr("src", data.path.replace(/\\/g, "/"))
		$("#video_previev_tag")[0].play()
	}
}

function pausePrevievDisplay() {
	if (currentType == "video") {
		if (videoPaused == 0) {
			$("#video_previev_tag")[0].pause()
			videoPaused = 1
		} else if (videoPaused == 1) {
			$("#video_previev_tag")[0].play()
			videoPaused = 0
		}
	}
}

function stopPrevievDisplay() {
	clearTimeout(imageTimer);
	if (currentType == "image") {
		currentType = undefined
		$("#image_previev_div").fadeOut(10)
		$("#image_previev_tag").attr("src", "")
	} else if (currentType == "video") {
		currentType = undefined
		$("#video_previev_div").fadeOut(10)
		$("#video_previev_tag").attr("src", "")
		$("#video_previev_tag")[0].load()
		videoPaused = 0
	}
}

function getTimer(item_data) {
	$("#image_name").html(item_data.name)
	openImageTimerModal()
	return new Promise(resolve => {
		$("#save_timer").on("click", () => {
			let timer = $("#image_timer").val()
			resolve({
				add: "yes",
				time: timer
			})
			closeImageTimerModal()
		})
		$("#add_timer_modal").on("hide.bs.modal", () => {
			$("#save_timer").unbind("click");
			resolve({
				add: "no"
			})
			$("#add_timer_modal").unbind("hide.bs.modal")
		})
	})
}

function openImageTimerModal() {
	$("#image_timer").val(30)
	$("#add_timer_modal").modal("show")
}

function closeImageTimerModal() {
	$("#add_timer_modal").modal("hide")
}

function openStylingModal(id) {
	$("#blur_input").val(playlist[id].style.blur)
	$("#brightness_input").val(playlist[id].style.brightness)
	$("#contrast_input").val(playlist[id].style.contrast)
	$("#grayscale_input").val(playlist[id].style.grayscale)
	$("#huerotate_input").val(playlist[id].style.huerotate)
	$("#invert_input").val(playlist[id].style.invert)
	$("#opacity_input").val(playlist[id].style.opacity)
	$("#saturate_input").val(playlist[id].style.saturate)
	$("#sepia_input").val(playlist[id].style.sepia)
	$("#styling_modal").modal("show")
}

function closeStylingModal() {
	$("#styling_modal").modal("hide")
}

function clearPlaylist() {
	stop()
	refreshPlaylist()
	playlist = []
}

function markCurrent() {
	let list = document.getElementsByTagName("li")
	for (let index = 0; index < list.length; index++) {
		const element = list[index];
		const child = list[index].children[0].children[0]
		if (element.dataset.id == playlistItem) {
			if (child.className.includes("choosen")) {
				child.className = "choosen current"
			} else {
				child.className = "current"
			}
		} else {
			if (child.className.includes("choosen")) {
				child.className = "choosen"
			} else {
				child.className = ""
			}
		}
	}
}

function previous() {
	if (playlistItem - 1 < 0) {
		playlistItem = playlist.length - 1
		playItem(playlistItem)
		markCurrent()
	} else if (0 < playlistItem - 1 < playlist.length) {
		playlistItem = playlistItem - 1
		playItem(playlistItem)
		markCurrent()
	}
}

function playPause() {
	console.log("Play or Pause");
	if (mediaState == 0) {
		$("#play_pause_button").html('<i class="fad fa-pause"></i>')
		events.emit("pauseDisplay")
		pausePrevievDisplay()
		mediaState = 1
	} else if (mediaState == 1) {
		$("#play_pause_button").html('<i class="fad fa-play"></i>')
		events.emit("pauseDisplay")
		pausePrevievDisplay()
		mediaState = 0
	} else if (mediaState == 2) {
		$("#play_pause_button").html('<i class="fad fa-pause"></i>')
		events.emit("itemToDisplay", {
			type: playlist[playlistItem].type,
			path: playlist[playlistItem].path,
			style: playlist[playlistItem].style
		})
		itemToPreviev({
			type: playlist[playlistItem].type,
			path: playlist[playlistItem].path,
			style: playlist[playlistItem].style
		})
		mediaState = 1
	}

}

function stop() {
	console.log("Stop");
	events.emit("stopItemDisplay")
	playlistItem = 0
	mediaState = 2
	$("#play_pause_button").html('<i class="fad fa-play"></i> Play')
	stopPrevievDisplay()
	let itemsList = document.getElementsByTagName("li")
	for (let index = 0; index < itemsList.length; index++) {
		const element = itemsList[index];
		const child = itemsList[index].children[0].children[0];
		if (child.className.includes("choosen")) {
			child.className = "choosen"
		} else {
			child.className = ""
		}
	}
}

function next() {
	clearTimeout(imageTimer);
	if (playlist.length > playlistItem + 1) {
		playlistItem = playlistItem + 1
		playItem(playlistItem)
		markCurrent()
	} else if (playlist.length <= playlistItem + 1) {
		playlistItem = 0
		playItem(playlistItem)
		markCurrent()
	}
}

function repeat() {
	console.log("Repeat item or playlist");
	if (settings.repeatState == 0) {
		$("#repeat_button").html('<i class="fad fa-repeat"></i> <span>Loop playlist</span>')
		settings.repeatState = 1
	} else if (settings.repeatState == 1) {
		$("#repeat_button").html('<i class="fad fa-repeat-1"></i> <span>Loop item</span>')
		settings.repeatState = 2
	} else if (settings.repeatState == 2) {
		$("#repeat_button").html('<span>Repeat</span>')
		settings.repeatState = 0
	}
}

function refreshPlaylist() {
	let itemlist = document.getElementById("item_list")
	itemlist.innerHTML = ""
	for (let index = 0; index < playlist.length; index++) {
		const element = playlist[index];
		addToList(index, element.name)
	}
}

let el = document.getElementById('item_list');
let sortable = Sortable.create(el, {
	onEnd: function (evt) {
		let movedItem = playlist[evt.oldIndex]
		playlist.splice(evt.oldIndex, 1)
		playlist.splice(evt.newIndex, 0, movedItem)
		for (let index = 0; index < playlist.length; index++) {
			playlist[index].id = index
		}
		refreshPlaylist()
	},
	onChoose: function (evt) {
		let itemsList = document.getElementsByTagName("li")
		for (let index = 0; index < itemsList.length; index++) {
			const element = itemsList[index];
			const child = itemsList[index].children[0].children[0];

			if (child.className.includes("current")) {
				child.className = "current"
			} else {
				child.className = ""
			}
		}
		if (itemsList[evt.oldIndex].children[0].children[0].className.includes("current")) {
			itemsList[evt.oldIndex].children[0].children[0].className = "choosen current"
		} else {
			itemsList[evt.oldIndex].children[0].children[0].className = "choosen"
		}
		markCurrent()
	},
})

$('#video_previev_tag').on('ended', function () {
	checkOnNext()
});

function checkOnNext() {
	if (settings.repeatState == 1) {
		next()
	} else if (settings.repeatState == 2) {
		playItem(playlistItem)
	} else if (settings.repeatState == 0) {
		if (playlistItem >= playlist.length - 1) {
			stop()
		} else {
			next()
		}
	}
}
