const path = require('path');
const {
	app,
	BrowserWindow,
	screen,
	Event
} = require('electron');
const EventEmitter = require("events").EventEmitter
let controlWindow, displayWindow, previevWindow
app.Event = new EventEmitter();



const createControlWindow = async () => {
	const win = new BrowserWindow({
		show: false,
		width: 800,
		height: 600,
		webPreferences: {
			nodeIntegration: true,
			webSecurity: false,
			allowRunningInsecureContent: true
		},
		autoHideMenuBar: true
	});
	win.maximize();
	win.on('ready-to-show', () => {
		win.show();
	});

	win.on('closed', () => {
		onClose()
	});

	await win.loadFile(path.join(__dirname, '/files/windows/control/html/control.html'));

	return win;
};

const createDisplayWindow = async (disx, disy) => {
	const win = new BrowserWindow({
		show: false,
		width: 600,
		height: 400,
		x: disx,
		y: disy,
		fullscreen: true,
		alwaysOnTop: true,
		backgroundColor: '#000',
		webPreferences: {
			nodeIntegration: true,
			webSecurity: false,
			allowRunningInsecureContent: true
		},
		autoHideMenuBar: true
	});
	win.maximize()
	win.on('ready-to-show', () => {
		win.show();
	});

	win.on('closed', () => {
		onClose()
	});

	await win.loadFile(path.join(__dirname, '/files/windows/display/html/display.html'));

	return win;
};

const createPrevievWindow = async () => {
	const win = new BrowserWindow({
		show: false,
		width: 600,
		height: 400,
		backgroundColor: '#000',
		webPreferences: {
			nodeIntegration: true,
			webSecurity: false,
			allowRunningInsecureContent: true
		},
		autoHideMenuBar: true
	});
	win.on('ready-to-show', () => {
		win.show();
	});

	win.on('maximize', () => {
		win.maximize()
		setTimeout(() => {
			win.setFullScreen(true);
		}, 100);
	})
	win.on('closed', () => {
		onClose()
	});

	await win.loadFile(path.join(__dirname, '/files/windows/display/html/display.html'));

	return win;
};

function onClose() {
	controlWindow = undefined;
	previevWindow = undefined;
	displayWindow = undefined;
	app.quit();
}

app.on("window-all-closed", function () {
	if (process.platform !== "darwin") {
		app.quit();
	}
});


(async () => {
	await app.whenReady();
	controlWindow = await createControlWindow();
	let displays = screen.getAllDisplays()
	if (displays.length > 1) {
		for (let display = 1; display <= displays.length - 1; display++) {
			if (displays[display].bounds.x != 0) {
				if (displays[display].bounds.x > 0) {
					let disx = displays[display].bounds.x + 5
					if (displays[display].bounds.y != 0) {
						if (displays[display].bounds.y > 0) {
							let disy = displays[display].bounds.y + 5
							displayWindow = await createDisplayWindow(disx, disy);
						} else if (displays[display].bounds.y < 0) {
							let disy = displays[display].bounds.y - 5
							displayWindow = await createDisplayWindow(disx, disy);
						}
					} else if (displays[display].bounds.y == 0) {
						let disy = 0
						displayWindow = await createDisplayWindow(disx, disy);
					}
				} else if (displays[display].bounds.x < 0) {
					let disx = displays[display].bounds.x - 5
					if (displays[display].bounds.y != 0) {
						if (displays[display].bounds.y > 0) {
							let disy = displays[display].bounds.y + 5
							displayWindow = await createDisplayWindow(disx, disy);
						} else if (displays[display].bounds.y < 0) {
							let disy = displays[display].bounds.y - 5
							displayWindow = await createDisplayWindow(disx, disy);
						}
					} else if (displays[display].bounds.y == 0) {
						let disy = 0
						displayWindow = await createDisplayWindow(disx, disy);
					}
				}
			} else if (displays[display].bounds.x == 0) {
				let disx = 0
				if (displays[display].bounds.y > 0) {
					let disy = displays[display].bounds.y + 5
					displayWindow = await createDisplayWindow(disx, disy);

				} else if (displays[display].bounds.y < 0) {
					let disy = displays[display].bounds.y - 5
					displayWindow = await createDisplayWindow(disx, disy);
				}
			}

		}
	} else if (displays.length == 1) {
		previevWindow = await createPrevievWindow();
	}
})();

app.Event.on("test-message", (m) => {
	console.log(m);
})
